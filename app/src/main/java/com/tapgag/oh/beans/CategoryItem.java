package com.tapgag.oh.beans;

/**
 * Created by devmin-sikkle on 2016-04-01.
 */
public class CategoryItem {
    private String categoryNum = "";
    private String categoryName = "";
    private String categoryNameKr = "";
    private boolean isSelect = false;

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    public String getCategoryNum() {
        return categoryNum;
    }

    public void setCategoryNum(String categoryNum) {
        this.categoryNum = categoryNum;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryNameKr() {
        return categoryNameKr;
    }

    public void setCategoryNameKr(String categoryNameKr) {
        this.categoryNameKr = categoryNameKr;
    }
}
