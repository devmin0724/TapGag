package com.tapgag.oh.beans;

/**
 * Created by devmin on 2016-04-12.
 */
public class MypageItem {

    String title = "";
    String idx = "";

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdx() {
        return idx;
    }

    public void setIdx(String idx) {
        this.idx = idx;
    }
}
