package com.tapgag.oh.http.utils;

public class ServiceUtilities {
    
    public interface NetworkAvailabilityCallback{
        public void isNetworkAvailable(boolean flag);
    }

}
