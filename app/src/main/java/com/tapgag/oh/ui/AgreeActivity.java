package com.tapgag.oh.ui;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tapgag.oh.R;

public class AgreeActivity extends Activity {

    private int navi = 0;

    WebView wv;
    TextView tvTitle;
    LinearLayout layX;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agree);

        navi = getIntent().getExtras().getInt("navi");

        wv = (WebView) findViewById(R.id.wv_agree);

        tvTitle = (TextView) findViewById(R.id.tv_agree);

        layX = (LinearLayout) findViewById(R.id.lay_agree_x);
        layX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        switch (navi) {
            case 0:
                tvTitle.setText(getResources().getString(R.string.ui_Terms_of_Service));
                wv.loadUrl(getResources().getString(R.string.URL_Terms_of_Service));
                break;
            case 1:
                tvTitle.setText(getResources().getString(R.string.ui_Privacy_Policy));
                wv.loadUrl(getResources().getString(R.string.URL_Privacy_Policy));
                break;
        }
        //TODO 0 이용약관   1 개인정보
    }
}
