package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.beans.CategoryItem;
import com.tapgag.oh.beans.CommentItem;
import com.tapgag.oh.utils.TapGagApplication;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CategorySelectActivity extends Activity implements View.OnClickListener {

    LinearLayout layBack;
    ListView list;
    Button btn;
    int selectNavi = 0;

    List<CategoryItem> arr = new ArrayList<>();

    CategoryAapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_select);

        layBack = (LinearLayout) findViewById(R.id.lay_category_select_back);
        layBack.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list_category_select);

        btn = (Button) findViewById(R.id.btn_category_select);
        btn.setOnClickListener(this);

        for (int i = 1; i < MainActivity.cr.size(); i++) {
            arr.add(MainActivity.cr.get(i));
        }

        for (int i = 0; i < arr.size(); i++) {
            arr.get(i).setSelect(false);
        }

        adapter = new CategoryAapter(this, 0, arr);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < arr.size(); i++) {
                    arr.get(i).setSelect(false);
                }

                arr.get(position).setSelect(true);
                selectNavi = position + 1;

                adapter.update();
            }
        });
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_category_select_back:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.btn_category_select:
                if (selectNavi > 0) {
                    Intent i = new Intent();
                    i.putExtra("category", selectNavi);
                    setResult(RESULT_OK, i);
                    finish();
                }
                break;
        }
    }

    public class CategoryAapter extends ArrayAdapter<CategoryItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;
        List<CategoryItem> ifr;

        public CategoryAapter(Context ctx, int txtViewId, List<CategoryItem> modles) {
            super(ctx, txtViewId, modles);
            ifr = modles;
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return ifr.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_category, parent, false);

                TextView tv = (TextView) convertView.findViewById(R.id.tv_row_category);
                ImageView imv = (ImageView) convertView.findViewById(R.id.imv_row_category);

                if (TapGagApplication.sp.getString("country", "").equals("ko_KR")) {
                    tv.setText(ifr.get(pos).getCategoryNameKr());
                } else {
                    tv.setText(ifr.get(pos).getCategoryName());
                }

                imv.setSelected(ifr.get(pos).isSelect());

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }

        public void update() {
            viewArray.clear();
            notifyDataSetChanged();
        }
    }
}
