package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;

import org.json.JSONObject;

public class ChangeNickActivity extends Activity {

    LinearLayout layX, layEdit;
    EditText edt;
    ImageView imv;
    Button btn;
    boolean isOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_nick);

        layX = (LinearLayout) findViewById(R.id.lay_change_id_x);
        layX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layEdit = (LinearLayout) findViewById(R.id.lay_change_id);

        edt = (EditText) findViewById(R.id.edt_change_id);
        edt.addTextChangedListener(nickWatcher);

        edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_DONE){
                    if(isOk){
                        nickChange(edt.getText().toString());
                    }
                }
                return false;
            }
        });

        imv = (ImageView) findViewById(R.id.imv_change_id_check);

        btn = (Button) findViewById(R.id.btn_change_id);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isOk){
                    nickChange(edt.getText().toString());
                }
            }
        });

        edt.setText(TapGagApplication.sp.getString("nick", ""));
    }

    TextWatcher nickWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String temp = s.toString();
            if (temp.length() >= 4) {
                nickCheck(temp);
            } else {
                isOk = false;
                imv.setVisibility(View.INVISIBLE);
                layEdit.setBackgroundColor(getResources().getColor(R.color.login_error));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void nickCheck(String nick) {
        String postURL = getResources().getString(R.string.API_CHECK_NICK);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("nick", nick)
                .setResultReceiver(nickCheckHandler)
                .build();
        startService(intent);
    }

    private ResultHandler nickCheckHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));

            if (jd.getString("result").equals("0")) {
                isOk = true;
                imv.setVisibility(View.VISIBLE);
                layEdit.setBackgroundColor(Color.WHITE);
            }else{
                isOk = false;
                imv.setVisibility(View.INVISIBLE);
                layEdit.setBackgroundColor(getResources().getColor(R.color.login_error));
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };
    private ResultHandler nickChangeHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));

            if (jd.getString("result").equals("0")) {
                TapGagApplication.se.putString("nick", edt.getText().toString());
                TapGagApplication.se.commit();
                finish();
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void nickChange(String nick) {
        String postURL = getResources().getString(R.string.API_CHANGE_ID);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", TapGagApplication.sp.getString("id", ""))
                .withParam("nick", nick)
                .setResultReceiver(nickChangeHandler)
                .build();
        startService(intent);
    }
}
