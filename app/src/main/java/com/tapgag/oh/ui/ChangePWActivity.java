package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;

import org.json.JSONObject;

public class ChangePWActivity extends Activity {

    LinearLayout layX, layNow, layNew1, layNew2;
    ImageView checkNow, checkNew1, checkNew2;
    EditText edtNow, edtNew1, edtNew2;
    Button btn;

    boolean isNowOk = false, isNewSameOk = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pw);

        layX = (LinearLayout) findViewById(R.id.lay_change_pw_x);
        layX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        layNow = (LinearLayout) findViewById(R.id.lay_change_pass_now);
        layNew1 = (LinearLayout) findViewById(R.id.lay_change_pass1);
        layNew2 = (LinearLayout) findViewById(R.id.lay_change_pass2);

        checkNow = (ImageView) findViewById(R.id.imv_change_pass_now_check);
        checkNew1 = (ImageView) findViewById(R.id.imv_change_pass1_check);
        checkNew2 = (ImageView) findViewById(R.id.imv_change_pass2_check);

        edtNow = (EditText) findViewById(R.id.edt_change_pass_now);
        edtNow.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String temp = s.toString();
                if (TapGagApplication.sp.getString("pw", "").equals(temp)) {
                    isNowOk = true;
                    checkNow.setVisibility(View.VISIBLE);
                    layNow.setBackgroundColor(Color.WHITE);
                } else {
                    isNowOk = false;
                    checkNow.setVisibility(View.INVISIBLE);
                    layNow.setBackgroundColor(getResources().getColor(R.color.login_error));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        edtNew1 = (EditText) findViewById(R.id.edt_change_pass1);
        edtNew1.addTextChangedListener(newPassWatcher);
        edtNew2 = (EditText) findViewById(R.id.edt_change_pass2);
        edtNew2.addTextChangedListener(newPassWatcher);
        edtNew2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //TODO ㄱㄱ
                    if (isNowOk && isNewSameOk) {
                        pwChange(edtNew1.getText().toString());
                    }
                }
                return false;
            }
        });

        btn = (Button) findViewById(R.id.btn_change_pw);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO ㄱㄱ
                if (isNowOk && isNewSameOk) {
                    pwChange(edtNew1.getText().toString());
                }
            }
        });
    }

    TextWatcher newPassWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String temp = s.toString();
            if (temp.length() >= 4) {
                if (edtNew1.getText().toString().equals(edtNew2.getText().toString())) {
                    isNewSameOk = true;
                    checkNew1.setVisibility(View.VISIBLE);
                    checkNew2.setVisibility(View.VISIBLE);
                    layNew1.setBackgroundColor(Color.WHITE);
                    layNew2.setBackgroundColor(Color.WHITE);
                } else {
                    isNewSameOk = false;
                    checkNew1.setVisibility(View.INVISIBLE);
                    checkNew2.setVisibility(View.INVISIBLE);
                    layNew1.setBackgroundColor(getResources().getColor(R.color.login_error));
                    layNew2.setBackgroundColor(getResources().getColor(R.color.login_error));
                }
            } else {
                isNewSameOk = false;
                checkNew1.setVisibility(View.INVISIBLE);
                checkNew2.setVisibility(View.INVISIBLE);
                layNew1.setBackgroundColor(getResources().getColor(R.color.login_error));
                layNew2.setBackgroundColor(getResources().getColor(R.color.login_error));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private ResultHandler pwChangeHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                TapGagApplication.se.putString("pw", edtNew1.getText().toString());
                TapGagApplication.se.commit();
                finish();
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void pwChange(String pw) {
        String postURL = getResources().getString(R.string.API_CHANGE_PW);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", TapGagApplication.sp.getString("id", ""))
                .withParam("pw", pw)
                .setResultReceiver(pwChangeHandler)
                .build();
        startService(intent);
    }

}
