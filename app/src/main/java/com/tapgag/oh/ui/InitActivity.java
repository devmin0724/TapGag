package com.tapgag.oh.ui;

import android.annotation.TargetApi;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.ui.login.LoginActivity;
import com.tapgag.oh.utils.TapGagApplication;

import org.json.JSONObject;

import java.io.File;
import java.util.Locale;

public class InitActivity extends AppCompatActivity {

    private final int MY_PERMISSION_REQUEST_STORAGE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);

        findViewById(R.id.init_avloadingIndicatorView).setVisibility(View.VISIBLE);

        Locale systemLocale = getResources().getConfiguration().locale;
        String tempCountry = systemLocale.toString();
        Log.e("tempCountry", tempCountry);
        TapGagApplication.se.putString("country", tempCountry + "");
        TapGagApplication.se.commit();

        Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    checkPermission();
                } else {
                    getVersionData();
//                    start();
                }
            }
        };

        handler.sendEmptyMessageDelayed(0, 3000);
    }

    @TargetApi(Build.VERSION_CODES.M)
    private void checkPermission() {
        Log.i("per", "CheckPermission : " + checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE));

        if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (shouldShowRequestPermissionRationale(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(InitActivity.this, "Read/Write external storage & Camera permission", Toast.LENGTH_SHORT).show();
            }

            requestPermissions(new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.CAMERA}, MY_PERMISSION_REQUEST_STORAGE);
        } else {
            getVersionData();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSION_REQUEST_STORAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getVersionData();
//                    start();
                } else {
                    Toast.makeText(getApplicationContext(), "permission 승인 필요", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    public String getVersionName() {

        try {
            PackageInfo packageInfo = getApplicationContext()
                    .getPackageManager().getPackageInfo(
                            getApplicationContext().getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
//            Log.i(TAG, "NameNotFoundException => " + e.toString());
            return "";
        }

    }

    private ResultHandler versionhandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            String result = getStringFromArray(bs);
            JSONObject jd = new JSONObject(result);
            String serverVersion[] = jd.getString("version").split("/");
            if (serverVersion[0].equals(getVersionName()) || serverVersion[1].equals(getVersionName())) {
                start();
            } else {
                Intent i = new Intent();
                i.setAction(Intent.ACTION_VIEW);
                //TODO 패키지명 변경
                i.setData(Uri
                        .parse(getResources().getString(R.string.market_link)));
                startActivity(i);
                finish();
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void getVersionData() {
        String postURL = getResources().getString(R.string.API_GET_VERSION);

        Intent intent = new ServiceIntentBuilder(getApplication()).setData(Uri.parse(postURL)).setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .setResultReceiver(versionhandler).build();
        startService(intent);
    }

    private void start() {

        File f = new File(getExternalCacheDir() + "/tapgifs/");

        if (f.exists()) {

        } else {
            f.mkdirs();
        }
        //                startActivity(new Intent(InitActivity.this, ContentActivity.class));
        startActivity(new Intent(InitActivity.this, MainActivity.class));
//        startActivity(new Intent(InitActivity.this, LoginActivity.class));
        finish();
        overridePendingTransition(0, 0);
    }

}
