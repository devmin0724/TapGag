package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;

import java.text.SimpleDateFormat;
import java.util.Date;

public class LogoutPopup extends Activity {

    TextView tv;
    Button btnNo, btnYes;
    int navi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logout_popup);

        tv = (TextView) findViewById(R.id.tv_popup_txt);

        navi = getIntent().getExtras().getInt("navi");

        switch (navi) {
            case 0:
                tv.setText(getResources().getString(R.string.ui_logout));
                break;
            case 1:
                tv.setText(getResources().getString(R.string.ui_signout));
                break;
        }

        btnNo = (Button) findViewById(R.id.btn_popup_no);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });

        btnYes = (Button) findViewById(R.id.btn_popup_yes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (navi) {
                    case 0:
                        logout();
                        break;
                    case 1:
                        signout();
                        break;
                }
            }
        });
    }

    private ResultHandler logoutHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            TapGagApplication.se.remove("al");
            TapGagApplication.se.remove("isLogin");
            TapGagApplication.se.remove("idx");
            TapGagApplication.se.remove("id");
            TapGagApplication.se.remove("pw");
            TapGagApplication.se.remove("gender");
            TapGagApplication.se.remove("year");
            TapGagApplication.se.remove("nick");
            TapGagApplication.se.remove("email");
            TapGagApplication.se.remove("type");
            TapGagApplication.se.remove("pushToken");
            TapGagApplication.se.remove("block");
            TapGagApplication.se.remove("date");
            TapGagApplication.se.commit();
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void logout() {
        String postURL = getResources().getString(R.string.API_LOGOUT);


        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", TapGagApplication.sp.getString("id", ""))
                .setResultReceiver(logoutHandler)
                .build();
        startService(intent);
    }

    private ResultHandler signoutHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            TapGagApplication.se.remove("al");
            TapGagApplication.se.remove("isLogin");
            TapGagApplication.se.remove("idx");
            TapGagApplication.se.remove("id");
            TapGagApplication.se.remove("pw");
            TapGagApplication.se.remove("gender");
            TapGagApplication.se.remove("year");
            TapGagApplication.se.remove("nick");
            TapGagApplication.se.remove("email");
            TapGagApplication.se.remove("type");
            TapGagApplication.se.remove("pushToken");
            TapGagApplication.se.remove("block");
            TapGagApplication.se.remove("date");
            TapGagApplication.se.commit();
            setResult(RESULT_OK);
            finish();
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void signout() {
        String postURL = getResources().getString(R.string.API_SIGNOUT);


        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", TapGagApplication.sp.getString("id", ""))
                .setResultReceiver(signoutHandler)
                .build();
        startService(intent);
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        finish();
    }
}
