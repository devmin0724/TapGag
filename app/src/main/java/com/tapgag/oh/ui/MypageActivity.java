package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.beans.CommentItem;
import com.tapgag.oh.beans.MypageItem;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MypageActivity extends Activity{

    LinearLayout layBack, laySetting;
    TextView tvNick, tvEmail, tvPostCount;
    ListView list;

    int pageNavi = 1;

    List<MypageItem> mpList = new ArrayList<>();

    MypageAdpater adpater;
    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mypage);

        ac = this;

        layBack = (LinearLayout) findViewById(R.id.lay_mypage_back);
        layBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        laySetting = (LinearLayout) findViewById(R.id.lay_mypage_settings);
        laySetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MypageActivity.this, SettingsActivity.class));
            }
        });

        tvNick = (TextView) findViewById(R.id.tv_mypage_nick);

        tvEmail = (TextView) findViewById(R.id.tv_mypage_email);

        tvPostCount = (TextView) findViewById(R.id.tv_mypage_post);

        list = (ListView) findViewById(R.id.list_mypage);

        tvNick.setText(TapGagApplication.sp.getString("nick", ""));

        tvEmail.setText(TapGagApplication.sp.getString("email", ""));

        getMypage();
    }

    private ResultHandler mypageHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jj = new JSONObject(getStringFromArray(bs));

            JSONArray jarr = new JSONArray(jj.getString("data"));

            JSONObject jd = new JSONObject(jj.getString("user"));

            tvPostCount.setText(jd.getString("post_count"));

            for (int i = 0; i < jarr.length(); i++) {
                MypageItem mi = new MypageItem();
                JSONObject j = jarr.getJSONObject(i);

                mi.setIdx(j.getString("idx"));
                mi.setTitle(j.getString("title"));

                mpList.add(mi);
            }
            adpater = new MypageAdpater(MypageActivity.this, 0, mpList);
            list.setAdapter(adpater);

        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void getMypage() {
        String postURL = getResources().getString(R.string.API_GET_MYPAGE);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", TapGagApplication.sp.getString("id", ""))
                .withParam("page", "" + pageNavi)
                .setResultReceiver(mypageHandler)
                .build();
        startService(intent);
    }

    public class MypageAdpater extends ArrayAdapter<MypageItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;
        List<MypageItem> ifr;

        public MypageAdpater(Context ctx, int txtViewId, List<MypageItem> modles) {
            super(ctx, txtViewId, modles);
            ifr = modles;
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return ifr.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_mypage, parent, false);

                TextView tvTitle = (TextView) convertView.findViewById(R.id.tv_row_mypage);

                tvTitle.setText(ifr.get(pos).getTitle());
//                tvTitle.setTag(ifr.get(pos).getIdx());

                convertView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(MypageActivity.this, SingleActivity.class);
                        i.putExtra("idx", ifr.get(pos).getIdx());
                        startActivity(i);
                    }
                });

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }

        public void update() {
            viewArray.clear();
            notifyDataSetChanged();
        }
    }
}
