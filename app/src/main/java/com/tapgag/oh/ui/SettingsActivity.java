package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.tapgag.oh.R;

public class SettingsActivity extends Activity implements View.OnClickListener{

    LinearLayout layBack, layId, layPw, layLogout, laySignout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        layBack = (LinearLayout)findViewById(R.id.lay_settings_back);
        layBack.setOnClickListener(this);

        layId = (LinearLayout)findViewById(R.id.lay_settings_change_id);
        layId.setOnClickListener(this);

        layPw = (LinearLayout)findViewById(R.id.lay_settings_change_pw);
        layPw.setOnClickListener(this);

        layLogout = (LinearLayout)findViewById(R.id.lay_settings_logout);
        layLogout.setOnClickListener(this);

        laySignout = (LinearLayout)findViewById(R.id.lay_settings_signout);
        laySignout.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i3 = new Intent(SettingsActivity.this, LogoutPopup.class);
        switch (v.getId()){
            case R.id.lay_settings_back :
                finish();
                break;
            case R.id.lay_settings_change_id :
                startActivity(new Intent(SettingsActivity.this, ChangeNickActivity.class));
                break;
            case R.id.lay_settings_change_pw :
                startActivity(new Intent(SettingsActivity.this, ChangePWActivity.class));
                break;
            case R.id.lay_settings_logout :
                i3.putExtra("navi", 0);
                startActivityForResult(i3, 1111);
                break;
            case R.id.lay_settings_signout :
                i3.putExtra("navi", 1);
                startActivityForResult(i3, 2222);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1111){
            if(resultCode == RESULT_OK){
                MypageActivity.ac.finish();
                finish();
            }
        }else if(requestCode == 2222){
            if(resultCode == RESULT_OK){
                MypageActivity.ac.finish();
                finish();
                MainActivity.ac.finish();
                startActivity(new Intent(SettingsActivity.this, InitActivity.class));
            }
        }
    }
}
