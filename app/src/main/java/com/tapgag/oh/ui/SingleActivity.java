package com.tapgag.oh.ui;

import android.app.Activity;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.tapgag.oh.R;
import com.tapgag.oh.beans.CategoryItem;
import com.tapgag.oh.beans.CategoryResult;
import com.tapgag.oh.beans.CommentItem;
import com.tapgag.oh.beans.PostItem;
import com.tapgag.oh.cache.FileCache;
import com.tapgag.oh.cache.GIFLoader;
import com.tapgag.oh.cache.ImageFullLoader;
import com.tapgag.oh.cache.VideoLoader;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.ui.login.LoginConfirmActivity;
import com.tapgag.oh.utils.TapGagApplication;
import com.tapgag.oh.widget.BoundableOfflineImageView;
import com.tapgag.oh.widget.VView;
import com.tapgag.oh.widget.quick.enums.QuickReturnViewType;
import com.tapgag.oh.widget.quick.listeners.QuickReturnListViewOnScrollListener;
import com.tapgag.oh.widget.quick.utils.TransAPI;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class SingleActivity extends Activity implements View.OnClickListener {

    private LayoutInflater inflater;
    private View headerMain, quickView;
    private int tempPlayCount = 0;
    //    private RecyclerView rv;
    private ListView list;
    private PostItem pi = new PostItem();

    BoundableOfflineImageView bimv;
    TextView title, tvLikeCount, tvPlayCount, tvComCount;
    ImageView imvLike, imvDown, imvShare;
    VView vv;
    FrameLayout layInput;
    LinearLayout layComment, layLoading, layBack;
    AVLoadingIndicatorView av;
    TextView tvSend;
    EditText edtComment;

    private String tempPostId = "";

    private long latestId = -1;

    private DownloadManager downloadManager;
    private DownloadManager.Request request;
    private Uri urlToDownload;

    private String url = "";
    private String urlGIF = "";
    private String idx = "";

    boolean isLike = false;

    public static ProgressDialog mProgressDialog;

    BroadcastReceiver onComlete = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, getResources().getString(R.string.downlaod_complete), Toast.LENGTH_SHORT).show();
        }
    };

    VideoLoader videoLoader;
    ImageFullLoader imageFullLoader;

    List<CommentItem> cc = new ArrayList<>();

    CommentAdpater commentAdpater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single);

        idx = getIntent().getExtras().getString("idx");

        list = (ListView) findViewById(R.id.rv_single);

        inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        quickView = findViewById(R.id.lay_single_header);

        layLoading = (LinearLayout) findViewById(R.id.lay_single_loading);
        layLoading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        layBack = (LinearLayout) findViewById(R.id.lay_single_header_back);

        av = (AVLoadingIndicatorView) findViewById(R.id.av_single_send);
        tvSend = (TextView) findViewById(R.id.tv_single_comment_send);

        edtComment = (EditText) findViewById(R.id.edt_single_input_comment);

        edtComment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    av.setVisibility(View.VISIBLE);
                    tvSend.setVisibility(View.INVISIBLE);
                    sendComment();
                }
                return false;
            }
        });

        tvSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendComment();
            }
        });

        layComment = (LinearLayout) findViewById(R.id.lay_single_comment_edt);

        layInput = (FrameLayout) findViewById(R.id.lay_single_input);
        layInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layComment.getVisibility() == View.INVISIBLE) {
                    layComment.setVisibility(View.VISIBLE);
                }
            }
        });

        headerMain = inflater.inflate(R.layout.main_content, null);

        CommentItem ci1 = new CommentItem();
        cc.add(ci1);

        commentAdpater = new CommentAdpater(this, 0, cc);

        list.setAdapter(commentAdpater);

        list.addHeaderView(headerMain);

        getPostData();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_single_header_back:
                finish();
                break;
        }
    }

    private void headerSetting() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tempPostId = pi.getIdx();

                videoLoader = new VideoLoader(SingleActivity.this);
                imageFullLoader = new ImageFullLoader(SingleActivity.this);
                url = getResources().getString(R.string.RESOURCE_URL) + pi.getUrl() + ".mp4";
                urlGIF = getResources().getString(R.string.RESOURCE_URL) + pi.getUrl() + ".gif";

                bimv = (BoundableOfflineImageView) headerMain.findViewById(R.id.bimv_main_content);
                title = (TextView) headerMain.findViewById(R.id.tv_main_content_title);
                vv = (VView) headerMain.findViewById(R.id.vv);
                imvLike = (ImageView) headerMain.findViewById(R.id.imv_main_content_like);
                tvLikeCount = (TextView) headerMain.findViewById(R.id.tv_main_content_like_count);
                imvDown = (ImageView) headerMain.findViewById(R.id.imv_main_content_download);
                imvShare = (ImageView) headerMain.findViewById(R.id.imv_main_content_share);
                tvPlayCount = (TextView) headerMain.findViewById(R.id.tv_main_content_play_count);
                tvComCount = (TextView) headerMain.findViewById(R.id.tv_main_content_comment_count);

                downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);

                registerReceiver(onComlete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

                mProgressDialog = new ProgressDialog(SingleActivity.this);
                mProgressDialog.setMessage("Now Loading share for SNS");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);

                if (pi.getPost_type().equals("10001")) {
                    bimv.setVisibility(View.VISIBLE);
                    vv.setVisibility(View.GONE);
                    imageFullLoader.DisplayImage(getResources().getString(R.string.RESOURCE_URL) + pi.getUrl() + ".jpg", bimv, false);
                } else {
                    bimv.setVisibility(View.GONE);
                    vv.setVisibility(View.VISIBLE);
                    videoLoader.DisplayVideo(getResources().getString(R.string.RESOURCE_URL) + pi.getUrl() + ".mp4", vv, false);
                }

                title.setText(pi.getTitle());

                imvLike.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isLike) {
                            TapGagApplication.setCount(tvLikeCount, Integer.parseInt(pi.getLike_count()) + 1);
                            sendLike();
                        }
                    }
                });

                imvShare.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendShare();
                    }
                });

                imvDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sendDonwload();
                    }
                });


                TapGagApplication.setCount(tvLikeCount, Integer.parseInt(pi.getLike_count()));
                TapGagApplication.setCount(tvPlayCount, Integer.parseInt(pi.getPlay_count()));
                TapGagApplication.setCount(tvComCount, Integer.parseInt(pi.getComment_count()));

                vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        int nowPlayCount = Integer.parseInt(pi.getPlay_count());
                        Log.e("onCom", nowPlayCount + "");
                        TapGagApplication.setCount(tvPlayCount, nowPlayCount + 1);
                        pi.setPlay_count(nowPlayCount + 1 + "");
                        tempPlayCount++;

                        mp.seekTo(0);
                        mp.start();
                    }
                });

                int headerHeight = quickView.getHeight();

                QuickReturnListViewOnScrollListener scrollListener = new QuickReturnListViewOnScrollListener.Builder(QuickReturnViewType.HEADER)
                        .header(quickView)
                        .minHeaderTranslation(-headerHeight)
                        .isSnappable(true)
                        .api(ta)
                        .build();

                Log.e("size", headerHeight + "");

                list.setOnScrollListener(scrollListener);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.topMargin = headerHeight;
                title.setLayoutParams(params);

                getCommentListInitData();
                layLoading.setVisibility(View.GONE);
            }
        });

    }

    TransAPI ta = new TransAPI() {
        @Override
        public void ok(int page) {
            getCommentListData(page);
        }
    };


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComlete);
    }

    private void shareItAll(final String url, BoundableOfflineImageView bimv) {
        GIFLoader gifLoader = new GIFLoader(this);
        gifLoader.DisplayImage(url, bimv, false);
        mProgressDialog.show();

        bimv.setOnImageChangedListener(new BoundableOfflineImageView.OnImageChangedListener() {
            @Override
            public void onImageChanged(Drawable d) {
                Intent sendFile = new Intent(Intent.ACTION_SEND);

                FileCache fc2 = new FileCache(getApplicationContext());

                File tempF = fc2.getFile(url);

                Uri uri = Uri.fromFile(tempF);

                sendFile.putExtra(Intent.EXTRA_STREAM, uri);
                sendFile.setType("image/gif");
                PackageManager pm = getPackageManager();
                List<ResolveInfo> activityList = pm.queryIntentActivities(sendFile, 0);
                sendFile.putExtra(Intent.EXTRA_STREAM, uri);
                sendFile.setType("image/gif");

                startActivity(sendFile);

                mProgressDialog.dismiss();
            }
        });
    }

    ResultHandler commentHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            Log.e("commentResult", getStringFromArray(bs));
            layLoading.setVisibility(View.GONE);
            JSONArray jarr = new JSONArray(getStringFromArray(bs));
            if (jarr.length() > 0) {
                for (int i = 0; i < jarr.length(); i++) {
                    CommentItem ci = new CommentItem();
                    JSONObject jd = jarr.getJSONObject(i);
                    ci.setId(jd.getString("writer"));
                    ci.setNick(jd.getString("nick"));
                    ci.setStr(jd.getString("str"));
                    ci.setDate(jd.getString("date"));
                    ci.setIdx(jd.getString("idx"));
                    ci.setPost_id(jd.getString("post_id"));
                    cc.add(ci);
                }

                Log.e("commentSize", cc.size() + "");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        commentAdpater = new CommentAdpater(SingleActivity.this, 0, cc);
                        commentAdpater.notifyDataSetChanged();
                        list.setAdapter(commentAdpater);
                    }
                });

            } else {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cc.clear();
                        cc.add(new CommentItem());
                        commentAdpater.notifyDataSetChanged();
                    }
                });
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {
            Log.e("fail aa", resultCode + " / " + e.toString());
        }
    };

    private void getCommentListInitData() {
        layLoading.setVisibility(View.VISIBLE);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                cc.clear();
                commentAdpater.notifyDataSetChanged();
            }
        });
        String postURL = getResources().getString(R.string.API_GET_COMMENT_LIST);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", tempPostId)
                .withParam("page", "1")
                .setResultReceiver(commentHandler)
                .build();
        startService(intent);
    }

    private ResultHandler commentSubmitHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {

            layLoading.setVisibility(View.GONE);
            tvSend.setVisibility(View.VISIBLE);
            av.setVisibility(View.INVISIBLE);

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            imm.hideSoftInputFromWindow(edtComment.getWindowToken(), 0);

            edtComment.setText("");
            layComment.setVisibility(View.INVISIBLE);

            getCommentListInitData();
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void sendComment() {
        layLoading.setVisibility(View.VISIBLE);
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_SEND_COMMENT);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", pi.getIdx())
                .withParam("str", edtComment.getText().toString())
                .withParam("writer", TapGagApplication.sp.getString("id", ""))
                .withParam("nick", TapGagApplication.sp.getString("nick", ""))
                .setResultReceiver(commentSubmitHandler)
                .build();
        startService(intent);
    }

    private ResultHandler shareHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            shareItAll(urlGIF, bimv);
            layLoading.setVisibility(View.GONE);
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void sendShare() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_SEND_SHARE);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", tempPostId)
                .setResultReceiver(shareHandler)
                .build();
        startService(intent);
    }

    private ResultHandler likeHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                isLike = true;
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private ResultHandler downloadHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);

            urlToDownload = Uri.parse(url);
            Log.e("downURL", url);
            List<String> pathSegments = urlToDownload.getPathSegments();
            request = new DownloadManager.Request(urlToDownload);
            request.setTitle("TapGag");
            request.setDescription("TapGag");
            request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, pathSegments.get(pathSegments.size() - 1));
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).mkdirs();
            latestId = downloadManager.enqueue(request);

        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void sendDonwload() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_SEND_DOWNLOAD);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", tempPostId)
                .setResultReceiver(downloadHandler)
                .build();
        startService(intent);
    }

    private void sendLike() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_SEND_LIKE);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", tempPostId)
                .setResultReceiver(likeHandler)
                .build();
        startService(intent);
    }

    private void getCommentListData(int page) {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_GET_COMMENT_LIST);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", tempPostId)
                .withParam("page", "" + page)
                .setResultReceiver(commentHandler)
                .build();
        startService(intent);
    }

    ResultHandler postHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));

            Log.e("result", getStringFromArray(bs));

            pi.setIdx(jd.getString("idx"));
            pi.setTitle(jd.getString("title"));
            pi.setBody(jd.getString("body"));
            pi.setUrl(jd.getString("url"));
            pi.setCategory(jd.getString("category"));
            pi.setPost_type(jd.getString("post_type"));
            pi.setLike_count(jd.getString("like_count"));
            pi.setComment_count(jd.getString("comment_count"));
            pi.setPlay_count(jd.getString("play_count"));
            pi.setDownload_count(jd.getString("download_count"));
            pi.setShare_count(jd.getString("share_count"));
            pi.setWriter(jd.getString("writer"));
            pi.setBlock(jd.getString("block"));
            pi.setDate(jd.getString("date"));

            headerSetting();
            layLoading.setVisibility(View.GONE);
//                headerAdapter.clear();
//                headerAdapter.add(new MainHeaderItem(MainActivity.this, pi));
//                rv.setAdapter(headerAdapter.wrap(adapter));

        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {
            Log.e("fail", resultCode + " / " + e.toString());
        }
    };

    private void getPostData() {
        layLoading.setVisibility(View.VISIBLE);
        isLike = false;
        tempPlayCount = 0;
        String postURL = getResources().getString(R.string.API_GET_SINGLE_POST);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("idx", "" + idx)
                .setResultReceiver(postHandler)
                .build();
        startService(intent);
    }

    private ResultHandler playCountHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                getPostData();
                layLoading.setVisibility(View.GONE);
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void sendPlayCount() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_SEND_PLAY_COUNT);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("post_id", pi.getIdx())
                .withParam("cnt", tempPlayCount + "")
                .setResultReceiver(playCountHandler)
                .build();
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (videoLoader != null) {
            videoLoader.DisplayVideo(getResources().getString(R.string.RESOURCE_URL) + pi.getUrl() + ".mp4", vv, false);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    public class CommentAdpater extends ArrayAdapter<CommentItem> {
        LayoutInflater m_LayoutInflater = null;
        SparseArray<WeakReference<View>> viewArray;
        List<CommentItem> ifr;

        public CommentAdpater(Context ctx, int txtViewId, List<CommentItem> modles) {
            super(ctx, txtViewId, modles);
            ifr = modles;
            this.viewArray = new SparseArray<WeakReference<View>>(modles.size());
            this.m_LayoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return ifr.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            final int pos = position;

            if (viewArray != null && viewArray.get(position) != null) {
                convertView = viewArray.get(position).get();
                if (convertView != null)
                    return convertView;
            }

            try {
                convertView = m_LayoutInflater.inflate(R.layout.row_comment, parent, false);

                TextView tvId = (TextView) convertView.findViewById(R.id.row_comment_id);
                TextView tvComment = (TextView) convertView.findViewById(R.id.row_comment_body);

                tvId.setText(ifr.get(pos).getNick());
                tvComment.setText(ifr.get(pos).getStr());

            } finally {
                viewArray.put(position, new WeakReference<View>(convertView));
            }
            return convertView;
        }

        public void update() {
            viewArray.clear();
            notifyDataSetChanged();
        }
    }
}
