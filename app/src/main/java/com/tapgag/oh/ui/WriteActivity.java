package com.tapgag.oh.ui;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fingram.qrb.QrBitmapFactory;
import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;
import com.tapgag.oh.widget.VView;

import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifDrawable;

public class WriteActivity extends Activity implements View.OnClickListener {

    private Uri mImageCaptureUri;
    private String title, url, category, post_type, writer, type;
    private LinearLayout layBack, layX;
    private VView vv;
    private ImageView imv;
    private EditText edt;
    private TextView tv;
    private Button btn;

    Animation shake;
    private String file_path;

    private File f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);

/*
        title = "Test Title";
        category = "1";
        post_type = "1002";
        writer = "devmin0724@gmail.com";
  */
        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        layBack = (LinearLayout) findViewById(R.id.lay_write_back);
        layBack.setOnClickListener(this);

        layX = (LinearLayout) findViewById(R.id.lay_write_x);
        layX.setOnClickListener(this);

        vv = (VView) findViewById(R.id.vv_write);

        vv.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.start();
            }
        });

        vv.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.seekTo(0);
                mp.start();
            }
        });

        imv = (ImageView) findViewById(R.id.imv_write);

        edt = (EditText) findViewById(R.id.edt_write);

        edt.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    categorySelect();
                }
                return false;
            }
        });

        edt.addTextChangedListener(countWatcher);

        tv = (TextView) findViewById(R.id.tv_write);

        btn = (Button) findViewById(R.id.btn_write);
        btn.setOnClickListener(this);

        writer = TapGagApplication.sp.getString("id", "");

        getContent();
    }

    private TextWatcher countWatcher = new TextWatcher() {
        String lastest = "";

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            lastest = s.toString();
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String tempS = s.toString();
            int byteLength = tempS.getBytes().length;
            if (byteLength > 140) {
                edt.setText(lastest);
            }
            count = 140 - byteLength;
            tv.setText(count + "");
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void getContent() {
        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
        i.setType("*/*");
        String[] mimetypes = {"image/*", "video/*"};
        i.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
        startActivityForResult(Intent.createChooser(i, "choose"), 1111);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1111) {
            if (resultCode == RESULT_OK) {
                String currnt = String.valueOf(System.currentTimeMillis());
                mImageCaptureUri = data.getData();

                type = getMimeType(mImageCaptureUri);

                if (type.equals("image/gif")) {
                    post_type = "1002";
                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + ".gif");
                    try {
                        GifDrawable gd = new GifDrawable(getContentResolver(), mImageCaptureUri);
                        imv.setImageDrawable(gd);
                        imv.setVisibility(View.VISIBLE);
                        vv.setVisibility(View.GONE);

                        try {
                            InputStream inputStream = getContentResolver().openInputStream(mImageCaptureUri);
                            OutputStream outStream = new FileOutputStream(f);

                            byte[] buf = new byte[1024];
                            int len = 0;
                            // 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
                            while ((len = inputStream.read(buf)) > 0) {
                                outStream.write(buf, 0, len);
                            }
                            // Stream 객체를 모두 닫는다.
                            outStream.close();
                            inputStream.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (type.equals("video/mp4")) {
                    post_type = "1003";
                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + ".mp4");
                    vv.setVideoURI(mImageCaptureUri);
                    imv.setVisibility(View.GONE);
                    vv.setVisibility(View.VISIBLE);

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(mImageCaptureUri);
                        OutputStream outStream = new FileOutputStream(f);

                        byte[] buf = new byte[1024];
                        int len = 0;
                        // 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
                        while ((len = inputStream.read(buf)) > 0) {
                            outStream.write(buf, 0, len);
                        }
                        // Stream 객체를 모두 닫는다.
                        outStream.close();
                        inputStream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else if (type.equals("image/jpeg")|| type.equals("image/jpg")
                        || type.equals("image/JPG") || type.equals("image/JPEG")) {

                    post_type = "1001";
//                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + "." + type.split("/")[type.split("/").length - 1]);
                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + ".jpg");

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(mImageCaptureUri);
                        OutputStream outStream = new FileOutputStream(f);

                        byte[] buf = new byte[1024];
                        int len = 0;
                        // 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
                        while ((len = inputStream.read(buf)) > 0) {
                            outStream.write(buf, 0, len);
                        }
                        // Stream 객체를 모두 닫는다.
                        outStream.close();
                        inputStream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    QrBitmapFactory.Options opt = new QrBitmapFactory.Options();

                    Bitmap b = QrBitmapFactory.decodeFile(getRealPathFromURI(mImageCaptureUri), opt);
                    imv.setImageBitmap(b);

                    imv.setVisibility(View.VISIBLE);
                    vv.setVisibility(View.GONE);
                }else if(type.equals("image/png") || type.equals("image/PNG")){
                    post_type = "1001";
//                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + "." + type.split("/")[type.split("/").length - 1]);
                    f = new File(getExternalCacheDir() + "/tapgifs/" + currnt + ".jpg");

                    try {
                        InputStream inputStream = getContentResolver().openInputStream(mImageCaptureUri);
                        OutputStream outStream = new FileOutputStream(f);

                        byte[] buf = new byte[1024];
                        int len = 0;
                        // 끝까지 읽어들이면서 File 객체에 내용들을 쓴다
                        while ((len = inputStream.read(buf)) > 0) {
                            outStream.write(buf, 0, len);
                        }
                        // Stream 객체를 모두 닫는다.
                        outStream.close();
                        inputStream.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Bitmap b = BitmapFactory.decodeFile(f.getAbsolutePath());

                    imv.setImageBitmap(b);

                    imv.setVisibility(View.VISIBLE);
                    vv.setVisibility(View.GONE);
                }

            }
        } else if (requestCode == 2222) {
            if (resultCode == RESULT_OK) {
                url = data.getExtras().getString("file");
                submitPost();
            }
        } else if (requestCode == 3333) {
            if (resultCode == RESULT_OK) {
                category = data.getExtras().getInt("category") + "";
                sendPost();
            }
        }
    }

    private String getMimeType(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String type = cR.getType(uri);
        Log.e("mime_extention", type + " / " + mime);
        return type;
    }

    private ResultHandler postHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                Toast.makeText(getApplicationContext(), "upload complete", Toast.LENGTH_LONG).show();
                finish();
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void submitPost() {
        String postURL = getResources().getString(R.string.API_SEND_POST);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("title", title)
                .withParam("url", url)
                .withParam("category", category)
                .withParam("post_type", post_type)
                .withParam("writer", TapGagApplication.sp.getString("id", ""))
                .setResultReceiver(postHandler)

                .build();
        startService(intent);
    }

    private void categorySelect() {
        if (edt.getText().toString().length() > 0) {
            title = edt.getText().toString();
            startActivityForResult(new Intent(WriteActivity.this, CategorySelectActivity.class), 3333);
        } else {
            edt.startAnimation(shake);
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void sendPost() {

        file_path = f.getAbsolutePath();
        Log.e("file", file_path);

        String[] fileNames = file_path.split("/");

        String fileName = fileNames[fileNames.length - 1];

        Intent i = new Intent(WriteActivity.this, FileUploadActivity.class);
        i.putExtra("url", file_path);
        i.putExtra("file", fileName);

        startActivityForResult(i, 2222);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_write:
                categorySelect();
                break;
            case R.id.lay_write_back:
                getContent();
                break;
            case R.id.lay_write_x:
                finish();
                break;
        }
    }
}
