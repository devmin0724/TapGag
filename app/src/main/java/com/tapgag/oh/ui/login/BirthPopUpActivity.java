package com.tapgag.oh.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;

import com.tapgag.oh.R;

import java.util.Calendar;

public class BirthPopUpActivity extends Activity implements View.OnClickListener{

    NumberPicker np;
    Button btnCancel, btnConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birth_pop_up);

        np = (NumberPicker)findViewById(R.id.np);
        np.setMinValue(1900);
        np.setMaxValue(Calendar.getInstance().get(Calendar.YEAR));
        np.setValue(Calendar.getInstance().get(Calendar.YEAR) - 26);

        setDividerColor(np, getResources().getColor(R.color.numberpicker_divider));

        btnCancel = (Button)findViewById(R.id.btn_birth_no);
        btnCancel.setOnClickListener(this);
        btnConfirm = (Button)findViewById(R.id.btn_birth_yes);
        btnConfirm.setOnClickListener(this);

//        ((Paint)selectorWheelPaintField.get(NumberPicker)).setColor(color);
    }

    private void setDividerColor(NumberPicker picker, int color) {

        java.lang.reflect.Field[] pickerFields = NumberPicker.class.getDeclaredFields();
        for (java.lang.reflect.Field pf : pickerFields) {
            if (pf.getName().equals("mSelectionDivider")) {
                pf.setAccessible(true);
                try {
                    ColorDrawable colorDrawable = new ColorDrawable(color);
                    pf.set(picker, colorDrawable);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (Resources.NotFoundException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    @Override
    public void onBackPressed() {
        finish();
        setResult(RESULT_CANCELED);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_birth_no :
                finish();
                setResult(RESULT_CANCELED);
                break;
            case R.id.btn_birth_yes :
                Intent i = new Intent();
                i.putExtra("num", np.getValue());
                setResult(RESULT_OK, i);
                finish();
                break;
        }
    }
}
