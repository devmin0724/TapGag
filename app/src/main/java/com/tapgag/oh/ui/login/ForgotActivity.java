package com.tapgag.oh.ui.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;

import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

public class ForgotActivity extends Activity implements View.OnClickListener {

    LinearLayout layForgot, layLoading, layEdt;
    EditText edt;
    TextView tv;
    Button btnCancel, btnConfirm;
    boolean isOK = false;

    List<List<String>> passs = Arrays.asList(
            Arrays.asList("p", "i", "c", "p", "i", "c", "1", "2", "3", "4", "5"),
            Arrays.asList("c", "h", "r", "i", "s", "o", "h", "6", "7", "8", "9", "0"),
            Arrays.asList("d", "e", "v", "m", "i", "n", "1", "2", "3", "4", "5"),
            Arrays.asList("j", "o", "x", "s", "u", "m", "6", "7", "8", "9", "0"),
            Arrays.asList("t", "a", "p", "g", "a", "g", "1", "2", "3", "4", "5")
    );

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_forgot);

        layForgot = (LinearLayout) findViewById(R.id.lay_forgot);
        layLoading = (LinearLayout) findViewById(R.id.lay_forgot_loading);

        layEdt = (LinearLayout)findViewById(R.id.lay_forgot_edt);

        edt = (EditText) findViewById(R.id.edt_forgot_email);
        tv = (TextView) findViewById(R.id.tv_forgot_txt);
        btnCancel = (Button) findViewById(R.id.btn_forgot_no);
        btnConfirm = (Button) findViewById(R.id.btn_forgot_yes);

        btnCancel.setOnClickListener(this);
        btnConfirm.setOnClickListener(this);

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(edt, InputMethodManager.SHOW_FORCED);
    }

    ResultHandler checkEmailHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            isOK = true;
            layLoading.setVisibility(View.GONE);
            Log.e("tempREsult", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            String tempResult = jd.getString("result");
            if (tempResult.equals("0")) {
                //이메일 발송완료
                layEdt.setVisibility(View.GONE);
                tv.setVisibility(View.VISIBLE);
                tv.setText(edt.getText().toString() + "\n" + getResources().getString(R.string.ui_forgot_success));
            } else if (tempResult.equals("2")) {
                //TODO 가입된 이메일 없음
                layEdt.setVisibility(View.GONE);
                tv.setVisibility(View.VISIBLE);
                tv.setText(getResources().getString(R.string.ui_forgot_no_email));
            } else if (tempResult.equals("3")) {
                //TODO 페북
                layEdt.setVisibility(View.GONE);
                tv.setVisibility(View.VISIBLE);
                tv.setText(getResources().getString(R.string.ui_forgot_face));
            } else {
                finish();
                setResult(RESULT_CANCELED);
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private String getPassword() {
        String tempPass = "";
        for (int i = 0; i < passs.size(); i++) {
            tempPass += passs.get(i).get((int) (Math.random() * passs.get(i).size()));
        }
        Log.e("tempPass", tempPass);
        return tempPass;
    }

    private void getCheckEmail() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_FOGOT);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("email", edt.getText().toString())
                .withParam("pass", getPassword())
                .setResultReceiver(checkEmailHandler)
                .build();
        startService(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_forgot_no:
                finish();
                setResult(RESULT_CANCELED);
                break;
            case R.id.btn_forgot_yes:
                if (isOK) {
                    finish();
                    setResult(RESULT_CANCELED);
                } else {
                    if (edt.getText().toString().length() < 1) {

                    } else {
                        getCheckEmail();
                    }
                }
                break;
        }
    }
}
