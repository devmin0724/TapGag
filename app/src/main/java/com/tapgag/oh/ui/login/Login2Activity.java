package com.tapgag.oh.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.ui.MainActivity;
import com.tapgag.oh.utils.TapGagApplication;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login2Activity extends Activity implements View.OnClickListener {

    EditText edtEmail, edtPass;
    ImageView imvEmailCheck, imvPassWordCheck;
    LinearLayout layEmail, layPass;
    Button btn;
    boolean isEmail = false, isPass = false;
    Animation shake;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        edtEmail = (EditText) findViewById(R.id.edt_login2_email);
        edtEmail.addTextChangedListener(emailWatcher);
        edtPass = (EditText) findViewById(R.id.edt_login2_pass1);
        edtPass.addTextChangedListener(passWatcher);

        imvEmailCheck = (ImageView) findViewById(R.id.imv_login2_email_check);
        imvPassWordCheck = (ImageView) findViewById(R.id.imv_login2_pass1_check);

        layEmail = (LinearLayout) findViewById(R.id.lay_login2_email);
        layPass = (LinearLayout) findViewById(R.id.lay_login2_pass1);

        btn = (Button) findViewById(R.id.btn_login2_confirm);
        btn.setOnClickListener(this);
        btn.setClickable(false);
        btn.setFocusable(false);

        edtEmail.setText(getIntent().getExtras().getString("id"));
        edtPass.requestFocus();
    }

    TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String temp = s.toString();
            isEmail = checkEmail(temp);
            checkResult();
            layEmail.setBackgroundColor(Color.WHITE);
            layPass.setBackgroundColor(Color.WHITE);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher passWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isPass = checkPass();
            checkResult();
            layEmail.setBackgroundColor(Color.WHITE);
            layPass.setBackgroundColor(Color.WHITE);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private boolean checkPass() {
        String tempPass = edtPass.getText().toString();
        if (!tempPass.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkEmail(String email) {

        if (email.length() > 45) {
            return false;
        }

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
//        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();

        return isNormal;
    }

    private void checkResult() {
        if (isEmail && isPass) {
            imvPassWordCheck.setVisibility(View.VISIBLE);
            imvEmailCheck.setVisibility(View.VISIBLE);
            btn.setClickable(true);
            btn.setFocusable(true);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit_c));
        } else {
            imvPassWordCheck.setVisibility(View.INVISIBLE);
            imvEmailCheck.setVisibility(View.INVISIBLE);
            if (isEmail) {
                imvEmailCheck.setVisibility(View.VISIBLE);
            }
            if (isPass) {
                imvPassWordCheck.setVisibility(View.VISIBLE);
            }
            btn.setClickable(false);
            btn.setFocusable(false);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit));
        }
    }

    private void checkLogin(int a) {
        switch (a) {
            case 0:
                layEmail.setBackgroundColor(Color.WHITE);
                layPass.setBackgroundColor(Color.WHITE);
                Toast.makeText(getApplicationContext(), "ok", Toast.LENGTH_SHORT).show();
                break;
            case 1:
                layEmail.setBackgroundColor(getResources().getColor(R.color.login_error));
                layPass.setBackgroundColor(getResources().getColor(R.color.login_error));
                layEmail.startAnimation(shake);
                layPass.startAnimation(shake);
                Toast.makeText(getApplicationContext(), "아이디와 비밀번호를 확인해주세요.", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    ResultHandler loginHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            Log.e("res", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {

                if (jd.isNull("block")) {
                    TapGagApplication.se.putBoolean("al", true);
                    TapGagApplication.se.putBoolean("isLogin", true);
                    TapGagApplication.se.putString("idx", jd.getString("idx"));
                    TapGagApplication.se.putString("id", jd.getString("id"));
                    TapGagApplication.se.putString("pw", edtPass.getText().toString());
                    TapGagApplication.se.putString("gender", jd.getString("gender"));
                    TapGagApplication.se.putString("year", jd.getString("year"));
                    TapGagApplication.se.putString("nick", jd.getString("nick"));
                    TapGagApplication.se.putString("email", jd.getString("email"));
                    TapGagApplication.se.putString("type", jd.getString("type"));
                    TapGagApplication.se.putString("pushToken", jd.getString("pushToken"));
                    TapGagApplication.se.putString("block", jd.getString("block"));
                    TapGagApplication.se.putString("date", jd.getString("date"));
                    TapGagApplication.se.commit();

                    setResult(RESULT_OK);
                    LoginActivity.ac.finish();
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "차단된 사용자 : " + jd.getString("block"), Toast.LENGTH_LONG).show();
                }
            }else if(jd.getString("result").equals("2")){
                Intent i = new Intent(Login2Activity.this, SignInActivity.class);
                i.putExtra("id", edtEmail.getText().toString());
                startActivity(i);
                finish();
            } else {
                checkLogin(1);
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void getLogin() {
        String postURL = getResources().getString(R.string.API_GET_LOGIN);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String tempDate = sdf.format(new Date());

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", edtEmail.getText().toString())
                .withParam("pw", edtPass.getText().toString())
                .withParam("type", "10001")
                .withParam("pushToken", "")
                .withParam("date", tempDate)
                .setResultReceiver(loginHandler)
                .build();
        startService(intent);
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(Login2Activity.this, LoginActivity.class);
        i.putExtra("id", edtEmail.getText().toString());
        startActivity(i);
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login2_confirm:
                //TODO 로그인 처리
                getLogin();
                break;
        }
    }
}
