package com.tapgag.oh.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.appevents.AppEventsLogger;
import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends Activity implements View.OnClickListener {

    LinearLayout layEmail, layFacebook, layLoading;
    EditText edt;
    ImageView imvEmailCheck, imvFaceIcon;
    TextView tvFace, tvForgot;
    Button btn;
    public static Activity ac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        ac = this;

        layEmail = (LinearLayout) findViewById(R.id.lay_login_email);
        layLoading = (LinearLayout)findViewById(R.id.lay_login);
        layFacebook = (LinearLayout) findViewById(R.id.lay_login_facebook);
        layFacebook.setOnClickListener(this);

        edt = (EditText) findViewById(R.id.edt_login_email);
        edt.addTextChangedListener(emailWatcher);

        imvEmailCheck = (ImageView) findViewById(R.id.imv_login_email_check);
        imvFaceIcon = (ImageView) findViewById(R.id.imv_login_facebook_icon);

        tvFace = (TextView) findViewById(R.id.tv_login_facebook_title);
        tvForgot = (TextView) findViewById(R.id.tv_login_forgot);
        tvForgot.setOnClickListener(this);

        btn = (Button) findViewById(R.id.btn_login_confirm);
        btn.setOnClickListener(this);
        btn.setClickable(false);

        edt.setText(getIntent().getExtras().getString("id"));

    }

    TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String temp = s.toString();
            emailCheckResult(checkEmail(temp));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void emailCheckResult(boolean is) {
        if (is) {
            imvEmailCheck.setVisibility(View.VISIBLE);
            btn.setClickable(true);
            btn.setFocusable(true);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit_c));
        } else {
            imvEmailCheck.setVisibility(View.INVISIBLE);
            btn.setClickable(false);
            btn.setFocusable(false);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit));
        }
    }

    public boolean checkEmail(String email) {

        if (email.length() > 45) {
            return false;
        }

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
//        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();

        return isNormal;
    }

    ResultHandler loginHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);
            Log.e("result", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if(jd.getString("result").equals("0")){
                //TODO 회원가입
                Intent i2 = new Intent(LoginActivity.this, SignInActivity.class);
                i2.putExtra("id", edt.getText().toString());
                startActivity(i2);
                finish();
            }else{
                Intent i = new Intent(LoginActivity.this, Login2Activity.class);
                i.putExtra("id", edt.getText().toString());
                startActivity(i);
                finish();
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void isLoginable() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_CHECK_EMAIL);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("email", edt.getText().toString())
                .setResultReceiver(loginHandler)
                .build();
        startService(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
//        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
//        AppEventsLogger.deactivateApp(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.lay_login_facebook:
                Toast.makeText(getApplicationContext(), "준비중", Toast.LENGTH_SHORT).show();
                break;
            case R.id.tv_login_forgot:
                startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
                break;
            case R.id.btn_login_confirm:
                isLoginable();
                break;
        }
    }
}
