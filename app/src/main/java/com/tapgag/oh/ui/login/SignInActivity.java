package com.tapgag.oh.ui.login;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tapgag.oh.R;
import com.tapgag.oh.http.HttpIntentService;
import com.tapgag.oh.http.ResultHandler;
import com.tapgag.oh.http.builders.ServiceIntentBuilder;
import com.tapgag.oh.utils.TapGagApplication;
import com.tapgag.oh.widget.Agree1;
import com.tapgag.oh.widget.Agree2;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SignInActivity extends Activity implements View.OnClickListener {

    private final int BIRTH_RETURN = 1111;

    LinearLayout layLoading, layEmail, layPass1, layPass2,
            layNick, layGender, layYear, layAgree;
    EditText edtEmail, edtPass1, edtPass2, edtNick;
    TextView tvGender, tvYear, tvAgree;
    ImageView imvEmail, imvPass1, imvPass2, imvNick;
    Button btn;
    Animation shake;
    boolean isBirth = false, isGender = false, gender = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        shake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);

        layLoading = (LinearLayout) findViewById(R.id.lay_signin_loading);
        layEmail = (LinearLayout) findViewById(R.id.lay_signin_email);
        layPass1 = (LinearLayout) findViewById(R.id.lay_signin_pass1);
        layPass2 = (LinearLayout) findViewById(R.id.lay_signin_pass2);
        layNick = (LinearLayout) findViewById(R.id.lay_signin_id);
        layGender = (LinearLayout) findViewById(R.id.lay_signin_gender);
        layGender.setOnClickListener(this);
        layYear = (LinearLayout) findViewById(R.id.lay_signin_birth);
        layYear.setOnClickListener(this);
        layAgree = (LinearLayout) findViewById(R.id.lay_signin_agree);

        edtEmail = (EditText) findViewById(R.id.edt_signin_email);
        edtEmail.addTextChangedListener(emailWatcher);
        edtPass1 = (EditText) findViewById(R.id.edt_signin_pass1);
        edtPass2 = (EditText) findViewById(R.id.edt_signin_pass2);
        edtPass2.addTextChangedListener(passWatcher);
        edtPass2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {

                }
            }
        });
        edtNick = (EditText) findViewById(R.id.edt_signin_id);
        edtNick.addTextChangedListener(nickWatcher);

        tvGender = (TextView) findViewById(R.id.tv_signin_gender);
        tvYear = (TextView) findViewById(R.id.tv_signin_birth);
        tvAgree = (TextView)findViewById(R.id.tv_singin_agree);

        imvEmail = (ImageView) findViewById(R.id.imv_signin_email_check);
        imvPass1 = (ImageView) findViewById(R.id.imv_signin_pass1_check);
        imvPass2 = (ImageView) findViewById(R.id.imv_signin_pass2_check);
        imvNick = (ImageView) findViewById(R.id.imv_signin_id_check);

        btn = (Button) findViewById(R.id.btn_signin_confirm);
        btn.setOnClickListener(this);
        btn.setClickable(false);
        btn.setFocusable(false);

        edtEmail.setText(getIntent().getExtras().getString("id"));

        edtPass1.requestFocus();

        String tagsText = tvAgree.getText().toString();

        ArrayList<int[]> hashtagSpans = getSpans(tagsText, getResources().getString(R.string.ui_signin_1));
        ArrayList<int[]> hashtagSpans2 = getSpans(tagsText, getResources().getString(R.string.ui_signin_2));

        SpannableString commentsContent2 = new SpannableString(tagsText);

        for (int i = 0; i < hashtagSpans.size(); i++) {
            int[] span = hashtagSpans.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            commentsContent2.setSpan(new Agree1(this),
                    hashTagStart, hashTagEnd, 0);
        }
        for (int i = 0; i < hashtagSpans2.size(); i++) {
            int[] span = hashtagSpans2.get(i);
            int hashTagStart = span[0];
            int hashTagEnd = span[1];

            commentsContent2.setSpan(new Agree2(this),
                    hashTagStart, hashTagEnd, 0);
        }

        tvAgree.setMovementMethod(LinkMovementMethod.getInstance());
        tvAgree.setText(commentsContent2);
        tvAgree.setHighlightColor(getResources().getColor(android.R.color.transparent));
    }

    TextWatcher nickWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkOK();
            String temp = s.toString();
            if (temp.length() >= 4) {
                imvNick.setVisibility(View.VISIBLE);
                layNick.setBackgroundColor(Color.WHITE);
            } else {
                imvNick.setVisibility(View.INVISIBLE);
                layNick.setBackgroundColor(getResources().getColor(R.color.login_error));
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    TextWatcher passWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkOK();
            String temp = s.toString();
            if (edtPass1.getText().toString().equals(temp)) {
                if (edtPass1.getText().toString().length() >= 4
                        && edtPass2.getText().toString().length() >= 4) {

                    layPass1.setBackgroundColor(Color.WHITE);
                    layPass2.setBackgroundColor(Color.WHITE);
                    imvPass1.setVisibility(View.VISIBLE);
                    imvPass2.setVisibility(View.VISIBLE);
                } else {
                    layPass1.setBackgroundColor(getResources().getColor(R.color.login_error));
                    layPass2.setBackgroundColor(getResources().getColor(R.color.login_error));
//                    layPass1.startAnimation(shake);
//                    layPass2.startAnimation(shake);
                }
            } else {
                layPass1.setBackgroundColor(getResources().getColor(R.color.login_error));
                layPass2.setBackgroundColor(getResources().getColor(R.color.login_error));
//                layPass1.startAnimation(shake);
//                layPass2.startAnimation(shake);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void checkOK() {

        if (checkEmail(edtEmail.getText().toString())
                && edtPass1.getText().toString().length() >= 4
                && edtPass2.getText().toString().length() >= 4
                && edtPass1.getText().toString().equals(edtPass2.getText().toString())
                && isBirth && isGender
                && edtNick.getText().toString().length() >= 4) {
            btn.setClickable(true);
            btn.setFocusable(true);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit_c));
        } else {
            btn.setClickable(false);
            btn.setFocusable(false);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit));
        }
    }

    private void emailCheckResult(boolean is) {
        if (is) {
            imvEmail.setVisibility(View.VISIBLE);
        } else {
            imvEmail.setVisibility(View.INVISIBLE);
            btn.setClickable(false);
            btn.setFocusable(false);
            btn.setBackgroundColor(getResources().getColor(R.color.login_submit));
        }
    }

    public boolean checkEmail(String email) {

        if (email.length() > 45) {
            return false;
        }

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
//        String regex = "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        boolean isNormal = m.matches();

        return isNormal;
    }

    TextWatcher emailWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            checkOK();
            String temp = s.toString();
            emailCheckResult(checkEmail(temp));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void check() {
        if (edtPass1.getText().toString().length() > 4) {
            if (edtPass2.getText().toString().length() > 4) {
                if (isGender) {
                    if (isBirth) {
                        checkNick();
                    } else {
                        layYear.startAnimation(shake);
                    }
                } else {
                    layGender.startAnimation(shake);
                }
            } else {
                layPass2.setBackgroundColor(getResources().getColor(R.color.login_error));
                layPass2.startAnimation(shake);
            }
        } else {
            layPass1.setBackgroundColor(getResources().getColor(R.color.login_error));
            layPass1.startAnimation(shake);
        }
    }

    private ResultHandler loginHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);
            Log.e("result", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                //TODO 회원가입 계속 진행
                check();
            } else {
                finish();
                Intent i = new Intent(SignInActivity.this, Login2Activity.class);
                i.putExtra("id", edtEmail.getText().toString());
                startActivity(i);
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void isLoginable() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_CHECK_EMAIL);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("email", edtEmail.getText().toString())
                .setResultReceiver(loginHandler)
                .build();
        startService(intent);
    }

    private ResultHandler joinHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);
            Log.e("result", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                //TODO 저장 및 메인 ㄱㄱ

                TapGagApplication.se.putBoolean("al", true);
                TapGagApplication.se.putBoolean("isLogin", true);
                TapGagApplication.se.putString("idx", jd.getString("idx"));
                TapGagApplication.se.putString("id", jd.getString("id"));
                TapGagApplication.se.putString("pw", jd.getString("pw"));
                TapGagApplication.se.putString("gender", jd.getString("gender"));
                TapGagApplication.se.putString("year", jd.getString("year"));
                TapGagApplication.se.putString("nick", jd.getString("nick"));
                TapGagApplication.se.putString("email", jd.getString("email"));
                TapGagApplication.se.putString("type", jd.getString("type"));
                TapGagApplication.se.putString("pushToken", jd.getString("pushToken"));
                TapGagApplication.se.putString("block", jd.getString("block"));
                TapGagApplication.se.putString("date", jd.getString("date"));
                TapGagApplication.se.commit();

                setResult(RESULT_OK);
                LoginActivity.ac.finish();
                finish();

            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void join() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_JOIN);

        String strGender = "";
        if (gender) {
            strGender = "1";
        } else {
            strGender = "0";
        }

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("id", edtEmail.getText().toString())
                .withParam("pw", edtPass1.getText().toString())
                .withParam("gender", strGender)
                .withParam("year", tvYear.getText().toString())
                .withParam("nick", edtNick.getText().toString())
                .withParam("type", "10001")
                .withParam("email", edtEmail.getText().toString())
                .withParam("pt", "")
                .setResultReceiver(joinHandler)
                .build();
        startService(intent);
    }

    private ResultHandler nickHandler = new ResultHandler() {
        @Override
        public void onSuccess(int resultCode, byte[] bs) throws Exception {
            layLoading.setVisibility(View.GONE);
            Log.e("result", getStringFromArray(bs));
            JSONObject jd = new JSONObject(getStringFromArray(bs));
            if (jd.getString("result").equals("0")) {
                //TODO ok 회원가입 넘겨
                join();
            } else {
                layNick.startAnimation(shake);
            }
        }

        @Override
        public void onError(int resultCode, byte[] bs) throws Exception {

        }

        @Override
        public void onFailure(int resultCode, Exception e) {

        }
    };

    private void checkNick() {
        layLoading.setVisibility(View.VISIBLE);
        String postURL = getResources().getString(R.string.API_CHECK_NICK);

        Intent intent = new ServiceIntentBuilder(getApplication())
                .setData(Uri.parse(postURL))
                .setHttpType(HttpIntentService.SERVICE_TYPE_GET)
                .withParam("nick", edtNick.getText().toString())
                .setResultReceiver(nickHandler)
                .build();
        startService(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signin_confirm:
                if (edtEmail.equals("")) {
                    layEmail.setBackgroundColor(getResources().getColor(R.color.login_error));
                    layEmail.startAnimation(shake);
                } else {
                    isLoginable();
                }
                break;
            case R.id.lay_signin_gender:
                isGender = true;
                checkOK();
                if (gender) {
                    gender = false;
                    tvGender.setText(getResources().getString(R.string.ui_signin_gender_f));
                    tvGender.setTextColor(0xff444444);
                } else {
                    gender = true;
                    tvGender.setText(getResources().getString(R.string.ui_signin_gender_m));
                    tvGender.setTextColor(0xff444444);
                }
                break;
            case R.id.lay_signin_birth:
                startActivityForResult(new Intent(SignInActivity.this, BirthPopUpActivity.class), BIRTH_RETURN);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == BIRTH_RETURN) {
            if (resultCode == RESULT_OK) {
                isBirth = true;
                checkOK();
                int num = data.getExtras().getInt("num");
                tvYear.setText(num + "");
                tvYear.setTextColor(0xff444444);
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(SignInActivity.this, LoginActivity.class);
        i.putExtra("id", edtEmail.getText().toString());
        startActivity(i);
        finish();
    }

    public ArrayList<int[]> getSpans(String body, String str) {
        ArrayList<int[]> spans = new ArrayList<>();

        Pattern pattern = Pattern.compile(str);
        Matcher matcher = pattern.matcher(body);

        while (matcher.find()) {
            int[] currentSpan = new int[2];
            currentSpan[0] = matcher.start();
            currentSpan[1] = matcher.end();
            spans.add(currentSpan);
        }

        return spans;
    }
}
