package com.tapgag.oh.utils;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.TextView;

import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpParams;

import java.text.DecimalFormat;

/**
 * Created by devmin-sikkle on 2016-03-31.
 */
public class TapGagApplication extends Application {

    public static final int API_GET_POSTS = 1;
    public static final int API_GET_CATEGORYS = 2;
    public static final int API_GET_COMMENTS = 3;
    public static final int API_SUBMIT_COMMENT = 4;
    public static final int API_SUBMIT_LIKE = 5;
    public static final int API_SUBMIT_PLAYCOUNT = 6;

    public static SharedPreferences sp;
    public static SharedPreferences.Editor se;

    @Override
    public void onCreate() {
        super.onCreate();

        sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        se = sp.edit();

    }

    public DefaultHttpClient getThreadSafeClient() {
        DefaultHttpClient client = new DefaultHttpClient();
        ClientConnectionManager mgr = client.getConnectionManager();
        HttpParams params = client.getParams();
        client = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
                mgr.getSchemeRegistry()), params);

        return client;
    }

    public static void setCount(TextView tv, int count) {
        DecimalFormat countDF = new DecimalFormat("0.#");

        String countStr;

        if (count >= 1000) {
            countStr = countDF.format(count / 1000) + "K";
        } else {
            countStr = count + "";
        }

        tv.setText(countStr);
    }
}
