package com.tapgag.oh.widget;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import com.tapgag.oh.ui.AgreeActivity;

/**
 * Created by devmin on 2016-04-20.
 */
public class Agree1  extends ClickableSpan {
    Context context;
    TextPaint textPaint;

    public Agree1(Context ctx) {
        super();
        context = ctx;
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        textPaint = ds;
        ds.setColor(ds.linkColor);
//        textPaint.bgColor = 0xfff5f5f5;
        textPaint.setTypeface(Typeface.defaultFromStyle(Typeface.BOLD));
        textPaint.setUnderlineText(true);
        textPaint.setARGB(255, 0, 0, 0);
        //textPaint.setARGB(255, 177, 177, 177);
    }

    @Override
    public void onClick(View widget) {
        TextView tv = (TextView) widget;
        Spanned s = (Spanned) tv.getText();
        int start = s.getSpanStart(this);
        int end = s.getSpanEnd(this);
//        String theWord = s.subSequence(start + 1, end).toString();
        Intent i = new Intent(context, AgreeActivity.class);
        i.putExtra("navi", 0);
        context.startActivity(i);
    }
}